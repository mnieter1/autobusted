# AutoBuSTeD

This is the public source code repository for the AutoBuSTeD project - For Automated Bubble Sweat Test and Diagnostics Software project.

The link to the project description on the institutes page can be found here https://bioinformatics.umg.eu/research/projects/autobusted/

