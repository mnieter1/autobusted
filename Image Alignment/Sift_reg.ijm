// visit all subfolders and run sift alignment
// on all images in a folder
//
// Paul Wuerzberg

requires("1.37e")
setBatchMode(true);
call("java.lang.System.gc");
dir = getDirectory( "Choose the Directory" );
openfolders(dir);

function openfolders(dir){
	list = getFileList( dir );
	x=0;
	for ( i=0; i<list.length; i++ ) {
    		if (endsWith(list[i], "/"))
    		{
    			openfolders(dir + list[i]);
    		}
		else{
			x=1;
		}
    		
	}
	if (x==1){processFiles(dir);}
}


function processFiles(dir) {
	list = getFileList( dir );
for ( i=0; i<list.length; i++ ) {
    open( dir + list[i] ); 
	}
	run("Images to Stack", "name=Stack title=[] use");
	run("Linear Stack Alignment with SIFT", "initial_gaussian_blur=1.60 steps_per_scale_octave=3 minimum_image_size=64 maximum_image_size=1024 feature_descriptor_size=4 feature_descriptor_orientation_bins=8 closest/next_closest_ratio=0.92 maximal_alignment_error=25 inlier_ratio=0.05 expected_transformation=Rigid interpolate");
	run("Stack to Images");
	//myDir = some_dir+"align"+File.separator;
	//File.makeDirectory(myDir);
	for ( i = list.length - 1; i>=0; i-- ) 
		{
		//saveAs("Jpeg", some_dir + "align/" + list[i]);
		saveAs("Jpeg", dir + "_alin_" + list[i]);
		close();
	}
	close();
	// gc after each round may be overkill
	call("java.lang.System.gc");
}